FROM node:latest

RUN npm install -g webpack@3.10.0

WORKDIR /usr/local/

COPY . .

RUN npm install

CMD ["webpack"]
