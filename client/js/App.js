'use strict';

import appStyles from '../sass/app.scss';
import Vue from 'vue'
import VueResource from 'vue-resource'
Vue.use(VueResource);

//-- Autocomplete component

const app = new Vue({
    el: '#app',
    data: {
        searchInput: '',
        states: [],
        selected: {
            name: null,
            abbreviation: null
        }
    },
    methods: {
        fetchAndFilter: function () {
            if (this.searchInput.replace(' ', '').length < 2) {
                this.states = [];
                return;
            }
            Vue.http.get(`/api/states?term=${this.searchInput}`).then(res => this.states = res.body.data);
        },
        setSelected: function (state) {
            this.selected = state;
            this.searchInput = this.selected.name;
            this.states = [];
        },
        clear: function () {
            this.searchInput = '';
            this.states = [];
        }
    }
})
